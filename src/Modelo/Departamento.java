package Modelo;

import ufps.util.colecciones_seed.ListaCD;

public class Departamento {

    private int id_dpto;

    private String nombreDpto;
    
    private ListaCD<Municipio> municipios=new ListaCD();

    public Departamento() {
    }
    
    
    

    public Departamento(int id_dpto, String nombreDpto) {
        this.id_dpto = id_dpto;
        this.nombreDpto = nombreDpto;
    }
    
    
    

    public int getId_dpto() {
        return id_dpto;
    }

    public void setId_dpto(int id_dpto) {
        this.id_dpto = id_dpto;
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public ListaCD<Municipio> getMunicipios() {
        return municipios;
    }

    public void setMunicipios(ListaCD<Municipio> municipios) {
        this.municipios = municipios;
    }

    @Override
    public String toString() {
        //return "Departamento{" + "id_dpto=" + id_dpto + ", nombreDpto=" + nombreDpto + ", municipios=" + municipios + '}';
        return "Departamento{" + "id_dpto=" + id_dpto + ", \t nombreDpto=" + nombreDpto+"\t Municipios:"+this.getListadoMunicipios()+" }\n\n";
    }
    
    
    public String getListadoMunicipios()
    {
        return this.municipios.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.id_dpto;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Departamento other = (Departamento) obj;
        if (this.id_dpto != other.id_dpto) {
            return false;
        }
        return true;
    }
    
    
    
}
